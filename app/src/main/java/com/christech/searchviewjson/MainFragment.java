package com.christech.searchviewjson;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends Fragment
{
    public MainFragment()
    {
        // Required empty public constructor
    }

    RecyclerView NewsRecyclerView;
    SearchAdapter searchAdapter;
    List<SearchItemModel> mData;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput;
    CharSequence search = "";

    //ThatForJsonEndpoint
    public static final String URL="https://diveapi.herokuapp.com/api/getTopViewedDiveSites?top=50";
    RequestQueue requestQueue;
    private JSONArray result, resultThumb;
    String name, country, imageThumbnailUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        requestQueue = Volley.newRequestQueue(getContext());
        GettingData();
        fabSwitcher = view.findViewById(R.id.fab_switcherF);
        rootLayout = view.findViewById(R.id.root_layoutF);
        searchInput = view.findViewById(R.id.search_inputF);
        NewsRecyclerView = view.findViewById(R.id.news_rvF);
        mData = new ArrayList<>();

        isDark = getThemeStatePref();

        if (isDark)
        {
            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));
        }

        else
        {
            searchInput.setBackgroundResource(R.drawable.search_input_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }
        searchAdapter = new SearchAdapter(getContext(), mData, isDark);
        fabSwitcher.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fabSwitcherMethod();
            }
        });
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchAdapter.getFilter().filter(s);
                search = s;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private void fabSwitcherMethod()
    {
        isDark = !isDark;
        if (isDark)
        {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));
            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        }
        else
        {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            searchInput.setBackgroundResource(R.drawable.search_input_style);
        }
        searchAdapter = new SearchAdapter(getContext() , mData, isDark);
        if (!search.toString().isEmpty())
        {
            searchAdapter.getFilter().filter(search);
        }
        NewsRecyclerView.setAdapter(searchAdapter);
        saveThemeStatePref(isDark);
    }

    private void saveThemeStatePref(boolean isDark)
    {
        SharedPreferences preferences = getContext().getSharedPreferences("myPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isDark", isDark);
        editor.commit();
    }

    private boolean getThemeStatePref()
    {
        SharedPreferences preferences = getContext().getSharedPreferences("myPref", Context.MODE_PRIVATE);
        boolean isDark = preferences.getBoolean("isDark", false);
        return isDark;
    }

    private void GettingData()
    {
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    result = response.getJSONArray("dataResponse");
                    for (int i = 0; i< result.length(); i++)
                    {
                        name = result.getJSONObject(i).getString("name");
                        country = result.getJSONObject(i).getString("country");
                        mData.add(new SearchItemModel(name,country,0.0,R.drawable.divespoticon));
                    }

                    NewsRecyclerView.setAdapter(searchAdapter);
                    NewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                }catch (JSONException e)
                {
                    Toast.makeText(getContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getContext(), "Error:" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }
}
