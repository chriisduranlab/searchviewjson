package com.christech.searchviewjson;

import java.text.DecimalFormat;

public class SearchItemModel
{
    String NameSite, Country;
    int PhotoSiteIndex;
    Double Distance;

    public SearchItemModel()
    {

    }

    public String getNameSite()
    {
        return NameSite;
    }

    public void setNameSite(String nameSite)
    {
        NameSite = nameSite;
    }

    public String getCountry()
    {
        return Country;
    }

    public void setCountry(String country)
    {
        Country = country;
    }

    public Double getDistance()
    {
        return Distance;
    }

    public void setDistance(Double distance)
    {
        this.Distance = distance;
    }

    public int getPhotoSiteIndex()
    {
        return PhotoSiteIndex;
    }

    public void setPhotoSiteIndex(int photoSiteIndex)
    {
        this.PhotoSiteIndex = photoSiteIndex;
    }

    public SearchItemModel(String nameSite, String country, Double distance, int PhotoSiteIndex)
    {
        NameSite = nameSite;
        Country = country;
        this.Distance = distance;
        this.PhotoSiteIndex = PhotoSiteIndex;
    }
}
