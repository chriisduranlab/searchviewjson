package com.christech.searchviewjson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{

    RecyclerView NewsRecyclerView;
    SearchAdapter searchAdapter;
    List<SearchItemModel> mData;
    FloatingActionButton fabSwitcher;
    boolean isDark = false;
    RelativeLayout rootLayout;
    EditText searchInput;
    CharSequence search = "";

    //ThatForJsonEndpoint
    public static final String URL="https://diveapi.herokuapp.com/api/getTopViewedDiveSites?top=50";
    RequestQueue requestQueue;
    private JSONArray result, resultThumb;
    String name, country, imageThumbnailUrl;

    private FusedLocationProviderClient fusedLocationProviderClient;
    //ForGetNearPlace
    Double currentLat, currentLng;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        GettingGPS();
        GettingData();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        fabSwitcher = findViewById(R.id.fab_switcher);
        rootLayout = findViewById(R.id.root_layout);
        searchInput = findViewById(R.id.search_input);
        NewsRecyclerView = findViewById(R.id.news_rv);
        mData = new ArrayList<>();

        isDark = getThemeStatePref();
        if (isDark)
        {
            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));
        }
        else
        {
            searchInput.setBackgroundResource(R.drawable.search_input_style);
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
        }

        searchAdapter = new SearchAdapter(this, mData, isDark);
        fabSwitcher.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Fragment fragment = new MainFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.root_layout, fragment);
                fragmentTransaction.commit();

                //fabSwitcherMethod();
            }
        });

        searchInput.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                searchAdapter.getFilter().filter(s);
                search = s;
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    private void GettingGPS()
    {
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>()
        {
            @Override
            public void onSuccess(Location location)
            {
                if (location != null)
                {
                    currentLng = location.getLongitude();
                    currentLat = location.getLatitude();
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Location Null", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void fabSwitcherMethod()
    {
        isDark = !isDark;
        if (isDark)
        {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.black));
            searchInput.setBackgroundResource(R.drawable.search_input_dark_style);
        }
        else
        {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.white));
            searchInput.setBackgroundResource(R.drawable.search_input_style);
        }
        searchAdapter = new SearchAdapter(getApplicationContext() , mData, isDark);
        if (!search.toString().isEmpty())
        {
            searchAdapter.getFilter().filter(search);
        }
        NewsRecyclerView.setAdapter(searchAdapter);
        saveThemeStatePref(isDark);
    }

    private void GettingData()
    {
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    result = response.getJSONArray("dataResponse");
                    double km = 1000;
                    double resultado;
                    for (int i = 0; i< result.length(); i++)
                    {
                        name = result.getJSONObject(i).getString("name");
                        country = result.getJSONObject(i).getString("country");
                        String lat = result.getJSONObject(i).getString("lat");
                        String lng = result.getJSONObject(i).getString("lng");
                        Location locationA = new Location("point A");
                        locationA.setLatitude(currentLat);
                        locationA.setLongitude(currentLng);
                        Location locationB = new Location("point B");
                        locationB.setLatitude(Double.parseDouble(lat));
                        locationB.setLongitude(Double.parseDouble(lng));
                        Double distance = Double.valueOf(locationA.distanceTo(locationB));

                        //HereWeAreRoudingTheDistance
                        resultado = Math.round((distance/km) * 100.0)/ 100.0;
                        mData.add(new SearchItemModel(name,country, resultado , R.drawable.divespoticon));
                    }

                    NewsRecyclerView.setAdapter(searchAdapter);
                    NewsRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                }catch (JSONException e)
                {
                    Toast.makeText(MainActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(MainActivity.this, "Error:" + error, Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }

        private void saveThemeStatePref(boolean isDark)
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("myPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isDark", isDark);
        editor.commit();
    }

    private boolean getThemeStatePref()
    {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("myPref", MODE_PRIVATE);
        boolean isDark = preferences.getBoolean("isDark", false);
        return isDark;
    }
}
