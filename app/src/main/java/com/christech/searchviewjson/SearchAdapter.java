package com.christech.searchviewjson;

import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.NewsViewHolder> implements Filterable
{
    Context mContext;
    List<SearchItemModel> mData;
    List<SearchItemModel> mDataFiltered;
    boolean isDark = false;

    public SearchAdapter(Context mContext, List<SearchItemModel> mData, boolean isDark)
    {
        this.mContext = mContext;
        this.mData = mData;
        this.isDark = isDark;
        this.mDataFiltered = mData;
    }
    public SearchAdapter(Context mContext, List<SearchItemModel> mData)
    {
        this.mContext = mContext;
        this.mData = mData;
        this.mDataFiltered = mData;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View layout;
        layout = LayoutInflater.from(mContext).inflate(R.layout.cardviewlayout, parent, false);

        Collections.sort(mDataFiltered, new Comparator<SearchItemModel>()
        {
            @Override
            public int compare(SearchItemModel o1, SearchItemModel o2)
            {
                return Double.compare(o1.getDistance(), o2.getDistance());
            }
        });

        return new NewsViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position)
    {
        holder.img_user.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_transition_animation));
        holder.containter.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.fade_scale_animation));
        holder.tv_title.setText(mDataFiltered.get(position).getNameSite());
        holder.tv_content.setText(mDataFiltered.get(position).getCountry());
        holder.tv_date.setText(String.valueOf(mDataFiltered.get(position).getDistance()));
        holder.img_user.setImageResource(mDataFiltered.get(position).getPhotoSiteIndex());
    }

    @Override
    public int getItemCount()
    {
        return mDataFiltered.size();
    }

    @Override
    public Filter getFilter()
    {
        return new Filter()
        {
            @Override
            protected FilterResults performFiltering(CharSequence constraint)
            {
                String Key = constraint.toString();
                if (Key.isEmpty())
                {
                    mDataFiltered = mData;
                } else
                    {
                    List<SearchItemModel> lstFiltered = new ArrayList<>();
                    for (SearchItemModel row : mData) {
                        if (row.getNameSite().toLowerCase().contains(Key.toLowerCase()))
                        {
                            lstFiltered.add(row);
                        }
                    }
                    mDataFiltered = lstFiltered;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mDataFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results)
            {
                mDataFiltered = (List<SearchItemModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder
    {

        TextView tv_title, tv_content, tv_date;
        ImageView img_user;
        RelativeLayout containter;

        public NewsViewHolder(@NonNull View itemView)
        {
            super(itemView);
            containter = itemView.findViewById(R.id.container);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_content = itemView.findViewById(R.id.tv_description);
            tv_date = itemView.findViewById(R.id.tv_date);
            img_user = itemView.findViewById(R.id.img_user);

            if (isDark)
            {
                setDarkTheme();
            }
        }
        private void setDarkTheme()
        {
            containter.setBackgroundResource(R.drawable.card_bg_dark);
        }
    }
}
